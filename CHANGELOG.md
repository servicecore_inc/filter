# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 2.0.1 - 2020-05-08

### Added

- UniqueInSet filter

### Changed

- Nothing.

### Deprecated

- Nothing.

### Removed

- Nothing.

## 2.0.0 - 2020-01-08

### Added

- Nothing

### Changed

- Updated library to use Laminas instead of ZendFramework

### Deprecated

- Nothing.

### Removed

- ZendFramework dependencies

### Fixed

- Nothing.

## 1.0.0 - 2019-10-07

### Added

- Filter library!

### Changed

- Nothing.

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Nothing.
