# Filters

###### Phone

The phone filter is based on the `giggsey/libphonenumber` package..

A new filter can be created by instantiating a new `\ServiceCore\Filter\Phone` object, and passing in the PhoneNumber

###### UniqueValues

Removes duplicate values from a given array
