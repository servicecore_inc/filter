<?php

namespace ServiceCore\Filter;

use Laminas\Filter\AbstractFilter;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

class Phone extends AbstractFilter
{
    /** @var PhoneNumberUtil */
    private $phoneInstance;

    /** @var string */
    private $locale = 'US';

    /** @var int */
    private $outputMode = PhoneNumberFormat::E164;

    public function __construct()
    {
        $this->phoneInstance = PhoneNumberUtil::getInstance();
    }

    public function filter($value): ?string
    {
        if (!\is_string($value)) {
            return null;
        }

        try {
            $phone = $this->phoneInstance->parse($value, $this->locale);
        } catch (NumberParseException $e) {
            return $value;
        }

        return $this->phoneInstance->format($phone, $this->outputMode);
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getOutputMode(): int
    {
        return $this->outputMode;
    }

    public function setOutputMode(int $outputMode): self
    {
        $this->outputMode = $outputMode;

        return $this;
    }
}
