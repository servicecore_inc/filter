<?php

namespace ServiceCore\Filter;

use Laminas\Filter\AbstractFilter;

class UniqueValues extends AbstractFilter
{
    public function filter($value)
    {
        if (!\is_array($value)) {
            return $value;
        }

        return \array_unique($value, SORT_REGULAR);
    }
}
