<?php

namespace ServiceCore\Filter;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use PHPUnit\Framework\TestCase as Test;
use ReflectionClass;
use ServiceCore\Filter\Phone as PhoneFilter;

class PhoneTest extends Test
{
    /** @var array */
    private $data = [
        'raw'           => '(303) 586-4957',
        'filtered'      => '+13035864957',
        'invalidNumber' => 'foo',
    ];

    /** @var Phone */
    private $filter;

    public function setUp()
    {
        $this->filter = new PhoneFilter();
    }

    public function testConstructor(): void
    {
        $reflectedFilter = new ReflectionClass($this->filter);
        $phoneUtil       = $reflectedFilter->getProperty('phoneInstance');

        $phoneUtil->setAccessible(true);

        $this->assertInstanceOf(PhoneNumberUtil::class, $phoneUtil->getValue($this->filter));
    }

    public function testGetLocale(): void
    {
        $reflectedFilter = new ReflectionClass($this->filter);
        $locale          = $reflectedFilter->getProperty('locale');
        $localeValue     = 'en-US';

        $locale->setAccessible(true);
        $locale->setValue($this->filter, $localeValue);

        $this->assertEquals($localeValue, $this->filter->getLocale());
    }

    public function testSetLocale(): void
    {
        $reflectedFilter = new ReflectionClass($this->filter);
        $locale          = 'en-US';
        $localeProperty  = $reflectedFilter->getProperty('locale');

        $localeProperty->setAccessible(true);

        $this->assertSame($this->filter, $this->filter->setLocale($locale));
        $this->assertEquals($locale, $localeProperty->getValue($this->filter));
    }

    public function testGetOutputMode(): void
    {
        $reflectedFilter = new ReflectionClass($this->filter);
        $outputMode      = $reflectedFilter->getProperty('outputMode');
        $modeValue       = PhoneNumberFormat::E164;

        $outputMode->setAccessible(true);
        $outputMode->setValue($this->filter, $modeValue);

        $this->assertEquals($modeValue, $this->filter->getOutputMode());
    }

    public function testSetOutputMode(): void
    {
        $reflectedFilter = new ReflectionClass($this->filter);
        $outputMode      = $reflectedFilter->getProperty('outputMode');
        $modeValue       = PhoneNumberFormat::E164;

        $outputMode->setAccessible(true);

        $this->assertSame($this->filter, $this->filter->setLocale($modeValue));
        $this->assertEquals($modeValue, $outputMode->getValue($this->filter));
    }

    public function testFilterReturnsFilteredPhone(): void
    {
        $value = '7632689319';

        $this->assertEquals('+17632689319', $this->filter->filter($value));

        $value = '(763)268-9319';

        $this->assertEquals('+17632689319', $this->filter->filter($value));

        $value = '763268-9319';

        $this->assertEquals('+17632689319', $this->filter->filter($value));

        $this->assertEquals($this->data['filtered'], $this->filter->filter($this->data['raw']));
    }

    public function testFilterReturnsFilteredPhoneWithExtension(): void
    {
        $this->filter->setOutputMode(PhoneNumberFormat::NATIONAL);

        $this->assertEquals('(303) 867-5309 ext. 1234', $this->filter->filter('(303) 867 5309 x1234'));
    }

    public function testInvalidNumberReturnsNull(): void
    {
        $this->assertEquals($this->data['invalidNumber'], $this->filter->filter($this->data['invalidNumber']));

        $this->assertEquals(null, $this->filter->filter(null));

        $this->assertEquals(null, $this->filter->filter(false));

        $this->assertEquals(null, $this->filter->filter([]));
    }
}
