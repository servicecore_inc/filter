<?php

namespace ServiceCore\Filter\Tests;

use PHPUnit\Framework\TestCase as Test;
use ServiceCore\Filter\UniqueValues;

class UniqueValueTest extends Test
{
    /** @var UniqueValues */
    private $filter;

    public function setUp(): void
    {
        $this->filter = new UniqueValues();
    }

    public function testFilterDuplicatesPresent(): void
    {
        $value    = [1, 2, "aaa", "xc", 2, "aaa", 5];
        $filtered = $this->filter->filter($value);

        $this->assertCount(5, $filtered);
    }

    public function testFilterNoDuplicatesPresent(): void
    {
        $value    = [1, 2, "aaa", "xc"];
        $filtered = $this->filter->filter($value);

        $this->assertSameSize($value, $filtered);
    }

    public function testFilterEqualNestedArrays(): void
    {
        $value    = [[1, 2], ["aaa"], [1, 2]];
        $filtered = $this->filter->filter($value);

        $this->assertCount(2, $filtered);
        $this->assertCount(2, $filtered[0]);
        $this->assertCount(1, $filtered[1]);
    }

    public function testNonArrayValue(): void
    {
        $value    = "Just a random string";
        $filtered = $this->filter->filter($value);

        $this->assertEquals($value, $filtered);
    }

    public function testFilterEmptyValue(): void
    {
        $value    = [];
        $filtered = $this->filter->filter($value);

        $this->assertCount(0, $filtered);
    }

    public function testFilterNullValue(): void
    {
        $value    = null;
        $filtered = $this->filter->filter($value);

        $this->assertNull($filtered);
    }
}
